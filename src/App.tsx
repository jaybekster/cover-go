import Wizard from './features/wizard'
import './App.css'

function App() {
  return (
    <div className="App">
      <Wizard />
    </div>
  )
}

export default App
