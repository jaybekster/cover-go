import * as React from 'react'
import GreyBlock from '@/components/GreyBlock'
import Button from '@/components/Button'
import { useAppSelector, useAppDispatch } from '@/app/hooks'
import { COUNTRIES_MAP } from './constants'
import { Slide } from './types'
import { selectWizard, setSlide } from './slice'

type Page1Props = {
  onNext: () => void
  onPrev: () => void
}

const Slide3: React.FC<Page1Props> = (props) => {
  const wizard = useAppSelector(selectWizard)
  const dispatch = useAppDispatch()

  return (
    <GreyBlock>
      <h1>Summary</h1>
      <p>Name: {wizard.name}</p>
      <p>Age: {wizard.age}</p>
      <p>Where do you live: {COUNTRIES_MAP[wizard.currency]}</p>
      <p>Package: {wizard.package}</p>
      <p>
        Premium: {wizard.calculated}
        {wizard.currency}
      </p>

      <Button type="button" variant="secondary" onClick={props.onPrev}>
        Back
      </Button>
      <Button
        type="submit"
        variant="primary"
        onClick={() => dispatch(setSlide(Slide.Page1))}
      >
        Buy
      </Button>
    </GreyBlock>
  )
}

export default Slide3
