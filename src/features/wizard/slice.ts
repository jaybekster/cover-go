import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState, AppThunk } from '../../app/store'
import { Currency, Package, User, Slide } from './types'
import { SLIDES } from './constants'
import { calculate } from './utils'

export interface WizardState extends User {
  calculated: number
  slide: Slide
}

const initialState: WizardState = {
  name: '',
  age: 0,
  currency: Currency.HKD,
  package: Package.STANDARD,
  calculated: 0,
  slide: Slide.Page1,
}

export const counterSlice = createSlice({
  name: 'wizard',
  initialState,
  reducers: {
    setName: (state, action: PayloadAction<string>) => {
      state.name = action.payload
    },
    setAge: (state, action: PayloadAction<number>) => {
      state.age = action.payload
    },
    setCurrency: (state, action: PayloadAction<Currency>) => {
      state.currency = action.payload
    },
    setPackage: (state, action: PayloadAction<Package>) => {
      state.package = action.payload
    },
    setCalculated: (state, action: PayloadAction<number>) => {
      state.calculated = action.payload
    },
    setSlide: (state, action: PayloadAction<Slide>) => {
      state.slide = action.payload
    },
  },
})

export const {
  setName,
  setAge,
  setCurrency,
  setPackage,
  setCalculated,
  setSlide,
} = counterSlice.actions

export const calculatePayment = (): AppThunk => (dispatch, getState) => {
  const { age, currency } = selectWizard(getState())

  const payment = calculate(age, currency)

  dispatch(setCalculated(payment))
}

export const setField = ({
  key,
  value,
}: {
  [Key in keyof User]: {
    key: Key
    value: User[Key]
  }
}[keyof User]): AppThunk => (dispatch, getState) => {
  switch (key) {
    case 'age': {
      dispatch(setAge(value))
      dispatch(calculatePayment())
      break
    }
    case 'name': {
      dispatch(setName(value))
      break
    }
    case 'currency': {
      dispatch(setCurrency(value))
      break
    }
    case 'package': {
      dispatch(setPackage(value))
      dispatch(calculatePayment())
      break
    }
    default: {
      break
    }
  }
}

export const slideNext = (): AppThunk => (dispatch, getState) => {
  const { slide } = selectWizard(getState())

  const idx = SLIDES.indexOf(slide)

  if (idx > SLIDES.length) {
    return
  }

  dispatch(setSlide(SLIDES[idx + 1]))
}

export const slidePrev = (): AppThunk => (dispatch, getState) => {
  const { slide } = selectWizard(getState())

  const idx = SLIDES.indexOf(slide)

  if (idx < 0) {
    return
  }

  dispatch(setSlide(SLIDES[idx - 1]))
}

export const selectWizard = (state: RootState) => state.wizard

export default counterSlice.reducer
