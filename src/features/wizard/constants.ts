import { Option as SelectOption } from '@/components/Select'
import { Option as RadioOption } from '@/components/Radio'
import { Currency, Package, Slide } from './types'

export const COUNTRIES_MAP: Record<Currency, string> = {
  [Currency.HKD]: 'Hong Kong',
  [Currency.USD]: 'USA',
  [Currency.AUD]: 'Australia',
}

export const COUNTRIES: SelectOption<Currency>[] = [
  {
    id: Currency.HKD,
    value: 'Hong Kong',
  },
  {
    id: Currency.USD,
    value: 'USA',
  },
  {
    id: Currency.AUD,
    value: 'Australia',
  },
]

export const PACKAGES: RadioOption<Package>[] = [
  {
    id: Package.STANDARD,
    value: 'Standard',
  },
  {
    id: Package.SAFE,
    value:
      'Safe (it is 50% more expensive than Standard), it should show how much more user has to pay in the selected currency',
  },
  {
    id: Package.SUPER_SAFE,
    value:
      'Super Safe (it is 75% more expensive than Standard), it should show how much more user has to pay in the selected currency',
  },
]

export const RATES: Record<Currency, number> = {
  [Currency.HKD]: 1,
  [Currency.USD]: 2,
  [Currency.AUD]: 3,
}

export const SLIDES: [Slide, Slide, Slide] = [
  Slide.Page1,
  Slide.Page2,
  Slide.Page3,
]
