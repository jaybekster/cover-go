import * as React from 'react'
import { useAppSelector, useAppDispatch } from '@/app/hooks'
import { selectWizard, slideNext, slidePrev } from './slice'
import Slide1 from './Slide1'
import Slide2 from './Slide2'
import Slide3 from './Slide3'
import './index.css'
import { Slide } from './types'

const Wizard: React.FC = () => {
  const dispatch = useAppDispatch()
  const wizard = useAppSelector(selectWizard)
  const onNext = () => dispatch(slideNext())
  const onPrev = () => dispatch(slidePrev())

  let Content = null

  switch (wizard.slide) {
    case Slide.Page1:
      Content = <Slide1 onNext={onNext} />
      break
    case Slide.Page2:
      Content = <Slide2 onNext={onNext} onPrev={onPrev} />
      break
    case Slide.Page3:
      Content = <Slide3 onNext={onNext} onPrev={onPrev} />
      break
    default:
      return null
  }

  return <div className="wizard">{Content}</div>
}

export default Wizard
