import * as React from 'react'
import GreyBlock from '@/components/GreyBlock'
import Button from '@/components/Button'

type Page1Props = {
  onNext: () => void
}

const Page1: React.FC<Page1Props> = (props) => {
  return (
    <GreyBlock>
      <h1>Hello there</h1>
      <p>Let's buy some insurance. It is going to take only a few steps</p>
      <Button type="button" variant="primary" onClick={props.onNext}>
        Start
      </Button>
    </GreyBlock>
  )
}

export default Page1
