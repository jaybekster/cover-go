import * as React from 'react'
import InputText from '@/components/InputText'
import InputNumber from '@/components/InputNumber'
import Select from '@/components/Select'
import Radio from '@/components/Radio'
import GreyBlock from '@/components/GreyBlock'
import Button from '@/components/Button'
import { useAppSelector, useAppDispatch } from '@/app/hooks'
import { COUNTRIES, PACKAGES, SLIDES } from './constants'
import { User } from './types'
import { selectWizard, setField, slidePrev, slideNext, setSlide } from './slice'

type Page1Props = {
  onNext: () => void
  onPrev: () => void
}

const Slide2: React.FC<Page1Props> = (props) => {
  const dispatch = useAppDispatch()
  const wizard = useAppSelector(selectWizard)

  const [hasError, setHasError] = React.useState<boolean>(false)

  const onChangeHOF = React.useCallback(
    <K extends keyof User, V extends User[K]>(key: K) => (value: V) => {
      dispatch(
        setField({
          key,
          value,
        } as Parameters<typeof setField>[0]),
      )
    },
    [dispatch],
  )

  const handleSubmit = React.useCallback(
    (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault()

      if (wizard.age > 100) {
        setHasError(true)
      } else {
        dispatch(slideNext())
      }
    },
    [dispatch, wizard.age],
  )

  if (hasError) {
    return (
      <GreyBlock>
        <form>
          <h1>Ooops</h1>
          <p>Your age is over our accepted limit.</p>
          <p>We are sorry but we cannot insure you now.</p>
          <Button
            type="button"
            variant="primary"
            onClick={() => dispatch(setSlide(SLIDES[0]))}
          >
            Ok :(
          </Button>
        </form>
      </GreyBlock>
    )
  }

  return (
    <form onSubmit={handleSubmit}>
      <h1>Tell us about yourself</h1>
      <p>Let's buy some insurance. It is going to take only a few steps</p>
      <div>
        <InputText
          placeholder="Add text"
          label="Name"
          value={wizard.name}
          onChange={onChangeHOF('name')}
        />
      </div>
      <div>
        <InputNumber
          label="Age"
          value={wizard.age}
          onChange={onChangeHOF('age')}
        />
      </div>
      <div>
        <Select
          options={COUNTRIES}
          label="Where do you live"
          value={wizard.currency}
          onChange={onChangeHOF('currency')}
        />
      </div>
      <GreyBlock>
        <div>
          <Radio
            options={PACKAGES}
            value={wizard.package}
            onChange={onChangeHOF('package')}
          />
        </div>
        <div>
          <h2>Your premium is: {wizard.calculated}</h2>
        </div>
      </GreyBlock>
      <Button
        type="button"
        variant="secondary"
        onClick={() => dispatch(slidePrev())}
      >
        Back
      </Button>
      <Button type="submit" variant="primary">
        Next
      </Button>
    </form>
  )
}

export default Slide2
