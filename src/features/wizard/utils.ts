import { Currency } from './types'
import { RATES } from './constants'

export function calculate(age: number, currency: Currency): number {
  return age * 10 * RATES[currency]
}
