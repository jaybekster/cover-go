export enum Currency {
  HKD = 'HKD',
  USD = 'USD',
  AUD = 'AUD',
}

export enum Slide {
  'Page1',
  'Page2',
  'Page3',
}

export enum Package {
  STANDARD = 'STANDARD',
  SAFE = 'SAFE',
  SUPER_SAFE = 'SUPER-SAFE',
}

export interface User {
  age: number
  name: string
  package: Package
  currency: Currency
}
