import * as React from 'react'
import './index.css'

const GreyBlock: React.FC<{
  children: React.ReactNode
}> = (props) => {
  return <div className="grey-block">{props.children}</div>
}

export default GreyBlock
