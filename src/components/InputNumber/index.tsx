import * as React from 'react'

type InputNumberProps = {
  label: string
  placeholder?: string
  value?: number
  onChange?: (value: number) => void
}

// @todo correct parsing number

const InputNumber: React.FC<InputNumberProps> = (props) => {
  const onChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const value = parseFloat(event.target.value)

      if (isNaN(value)) {
        return
      }

      props.onChange?.(value)
    },
    [props.onChange],
  )

  return (
    <label>
      <div>{props.label}</div>
      <input
        pattern="[0-9]*\.?[0-9]*"
        type="text"
        value={props.value || ''}
        onChange={onChange}
      />
    </label>
  )
}

export default InputNumber
