import * as React from 'react'
import classNames from 'classnames'
import './index.css'

type ButtonProps = {
  type: React.ButtonHTMLAttributes<unknown>['type']
  children: React.ReactNode
  onClick?: () => void
  variant: 'primary' | 'secondary'
  disabled?: boolean
}

const Button: React.FC<ButtonProps> = (props) => {
  const className = classNames('button', {
    button_primary: props.variant === 'primary',
    button_secondary: props.variant === 'secondary',
    button_disabled: props.disabled,
  })

  return (
    <button
      className={className}
      type={props.type || 'button'}
      onClick={props.onClick}
      disabled={props.disabled}
    >
      {props.children}
    </button>
  )
}

export default Button
