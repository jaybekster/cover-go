import * as React from 'react'

export interface Option<T = string> {
  id: T
  value: string
}

type SelectProps<O extends Option<O['id']>> = {
  label: string
  value?: O['id']
  options: O[]
  onChange?: (id: O['id']) => void
}

function Select<O extends Option<O['id']>>(props: SelectProps<O>) {
  const onChange = React.useCallback(
    (event: React.ChangeEvent<HTMLSelectElement>) => {
      const value = event.target.value

      console.log(value)

      props.onChange?.(value)
    },
    [props.onChange],
  )

  return (
    <label>
      <div>{props.label}</div>
      <select onChange={onChange} value={(props.value as string) || ''}>
        {props.options.map((option) => {
          return (
            <option key={option.id as string} value={option.id as string}>
              {option.value}
            </option>
          )
        })}
      </select>
    </label>
  )
}

export default Select
