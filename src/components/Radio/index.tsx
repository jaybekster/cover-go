import * as React from 'react'

export interface Option<T = string> {
  id: T
  value: string
}

type RadioProps<O extends Option<O['id']>> = {
  value: O['id']
  options: O[]
  onChange?: (id: O['id']) => void
}

function Radio<O extends Option<O['id']>>(props: RadioProps<O>) {
  const onChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const value = event.target.value

      props.onChange?.(value)
    },
    [props.onChange],
  )

  return (
    <fieldset>
      {props.options.map((option) => {
        return (
          <div key={option.id as string}>
            <label>
              <input
                type="radio"
                name="contact"
                value={option.id as string}
                checked={option.id === props.value}
                onChange={onChange}
              />
              {option.value}
            </label>
          </div>
        )
      })}
    </fieldset>
  )
}

export default Radio
