import * as React from 'react'

type InputTextProps = {
  label: string
  placeholder: string
  value?: string
  onChange?: (value: string) => void
}

const InputText: React.FC<InputTextProps> = (props) => {
  const onChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const value = event.target.value

      props.onChange?.(value)
    },
    [props.onChange],
  )

  return (
    <label>
      <div>{props.label}</div>
      <input type="text" value={props.value || ''} onChange={onChange} />
    </label>
  )
}

export default InputText
